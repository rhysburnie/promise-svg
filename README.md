Returns a promise which internally gets svg file via xhr and returns the svg as an element to be injected or consumed in some way.

Usage:
```
var promiseSVG = require('promise-svg');

var promise_that_resolves_an_svg_element = promiseSVG('path/to/svg.svg');

promise_that_resolves_an_svg_element.then(function(svg){
  document.body.appendChild(svg);
});
```

Catching errors:
```
promise_that_resolves_an_svg_element.catch(function(reason){
  // console.log(typeof reason === 'string');
  console.log(reason);
});
```