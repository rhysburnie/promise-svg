(function(root, promiseSVG){
  if(typeof module === "object" && module.exports) {
    module.exports = promiseSVG;
  } else {
    root.promiseSVG = promiseSVG;
  }
}(this, function promiseSVG(filepath, options) {
  var msgFailed = 'failed to load ' + filepath;
  var msgFailedSVG = filepath + ' is not an svg';

  options = options || {};

  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();

    xhr.open('GET', filepath, true);
    if (options.onprogress) {
      xhr.onprogress = options.onprogress;
    }
    xhr.onerror = function (e) {
      reject(msgFailed);
    }
    xhr.onload = function () {
      var svg, div = document.createElement('div');

      div.innerHTML = xhr.responseText;
      svg = div.getElementsByTagName('svg');
      div = null;
      if (!svg.length) {
        reject(msgFailedSVG);
      } else {
        resolve(svg[0]);
      }
    };
    try {
      xhr.send();
    } catch(err) {
      reject(msgFailed);
    }
  });
}));
